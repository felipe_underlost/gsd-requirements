﻿/// <binding AfterBuild='minification' ProjectOpened='watch' />
module.exports = function (grunt) {
    'use strict';

    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        bowercopy: {
            options: {
                runBower: true,
                destPrefix: 'Scripts/Vendors'
            },
            libs: {
                files: {
                    'angular': 'angular',
                    'jquery': 'jquery/dist',
                    'bootstrap': 'bootstrap/dist/css'
                }
            }
        },
        uglify: {
            my_target: {
                files: {
                    'wwwroot/Scripts/app.js': ['Scripts/App/**/*.js'],
                    'wwwroot/Scripts/vendor.js': ['wwwroot/lib/jquery/dist/jquery.js',
                                                  'wwwroot/lib/bootstrap/dist/js/bootstrap.js',
                                                  'wwwroot/lib/angular/angular.js',
                                                  'wwwroot/lib/ng-dialog/js/ngDialog.js']
                }
            }
        },
        cssmin: {
            concatenate: {
                files: {
                    'wwwroot/Content/vendors.min.css': ['Scripts/Vendors/**/*.css',
                                                        'wwwroot/lib/ng-dialog/css/ngDialog.css',
                                                        'wwwroot/lib/ng-dialog/css/ngDialog-theme-default.css'],
                    'wwwroot/Content/styles.min.css': ['Content/Styles/**/*.css', '!Content/Styles/**/*min.css']
                }
            }
        },
        watch: {
            js: {
                options: {
                    interrupt: true
                },
                files: ['Scripts/App/*/**.js'],
                tasks: ['uglify'],
                options: {
                    livereload: 51643,
                }
            },
            css: {
                options: {
                    interrupt: true
                },
                files: ['Content/Styles/*/**.css'],
                tasks: ['cssmin'],
                options: {
                    livereload: 51643,
                }
            }
        }
    });

    grunt.registerTask('minification', ['uglify', 'cssmin']);
    grunt.registerTask('bowerDependencies', ['bowercopy:libs']);

    grunt.loadNpmTasks('grunt-bowercopy');
    grunt.loadNpmTasks('grunt-karma');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-clean');
};