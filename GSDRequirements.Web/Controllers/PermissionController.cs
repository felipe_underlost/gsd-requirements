﻿using Microsoft.AspNet.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GSDRequirements.Web.Controllers
{
    public class PermissionController : Controller
    {
        // GET: /<controller>/
        public IActionResult UserManagement()
        {
            return View();
        }

        // GET: /<controller>/
        public IActionResult ProfileManagement()
        {
            return View();
        }
    }
}
